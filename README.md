# web-farm

# Структура проекта

```
group_vars/           # переменные для групп хостов dc1, dc2
roles/		       	# описание ролей для хостов
  ansible-users	    	# раскатка пользователей на хостах
  ol-yum-repos	    	# добавление репозиториев Oracle Linux
  webserver-backend 	# роль для backend/storage серверов и загрузки тестового сайта
  webserver-front   	# основная роль для конфигурации frontend серверов
  webserver-software	# базовая роль web сервера для установки ПО
vars/users.yml	    	# переменные для раскатки пользователей

ansible.cfg	      	# локальный конфиг ansible (задан inventory, параметры ssh)
hosts		      	# inventory файл

base-conf.yml	    	# playbook первоначальной настройки хостов
backend.yml	      	# playbook настройки backend/storage серверов
front.yml	      	# основной playbook для работы с frontend сервер
disaster-simulation.yml	# playbook для тестирования "кластера"
```

# Описание inventory файла

Роль сервера определяется группой:
* [dc?-front]
* [dc?-back]
* [dc?-stor]

Дополнительно группы объединены по распределению по Дата Центрам [dc1] и [dc2].
Для включения кэширования статики на frontend сервере задаётся переменная enable_cache=true,
может быть применено как к отдельному хосту, так и к группе.


front.yml и роль webserver-front
===

front.yml выполняет роль webserver-front для fron серверов обоих ДЦ.
Также в нём задаётся режим обслуживания 'backend_maintenance', меняющий состояние серверов upstream nginx.

```
roles/webserver-front/
  defaults/		# переменные для конфига nginx
  handlers/		# хук для рестарта nginx демона
  meta/			# указание зависимости от webserver-software
  tasks/		# процедура формирования конфигурации nginx
  templates/	       # шаблоны nginx.conf для двух ДЦ
```

# Howto

1. Подготовить ВМ. Обкатан запуск на Oracle Linux 7
2. Заполнить inventory
3. ansible-playbook base-conf.yml #Выполнить базовую настройку
4. ansible-playbook backend.yml
5. ansible-playbook front.yml
